unit DcadNumericEdit;

interface

uses
  System.SysUtils, System.Classes, System.UITypes, Winapi.Windows, Vcl.Controls, Vcl.StdCtrls, Vcl.Graphics, UInterfaces;

type

  NumType      = (Distance, DecDegrees, DegMinSec, IntegerNum, DecimalNum);

  TDcadNumericEdit = class(TCustomEdit)
  private
    function isValid : boolean;
    var datatype : NumType;
    var number : double;
    var allowNeg : boolean;
    var validClr : TColor;
    var errorClr : TColor;
    var initialTxt : string;
    procedure setNumber (num : double);   overload;
    procedure setNumber (num : integer);   overload;
    function getNumber : double;
    function getInt : integer;
    procedure SetClr (isValid : boolean);
    procedure AngToStr (ang : double;
                        var str : OpenString;
                        rel, roundit : boolean);
  protected
    procedure KeyPress(var Key: Char); override;
    procedure DoExit; override;
    procedure DoEnter; override;
  public
    constructor Create(AOwner: TComponent); override;
    property valid : boolean
      read isvalid;
    property NumValue : double
      read getnumber write setNumber;
    property IntValue : integer
      read getInt write setNumber;
  published
    property NumberType : NumType
      read dataType write dataType default Distance;
    property AllowNegative : boolean
      read allowNeg write allowNeg default true;
    property ColorError : TColor
      read errorClr write errorClr default clRed;

    { publish properties }
    property Align;
    property Alignment;
    property AlignWithMargins;
    property Anchors;
    property AutoSelect;
    property AutoSize;
    property BevelEdges;
    property BevelInner;
    property BevelKind;
    property BevelOuter;
    property BevelWidth;
    property BiDiMode;
    property BorderStyle;
    property Color;
    property Constraints;
    property Ctl3D;
    property Cursor;
    property CustomHint;
    property Enabled;
    property Font;
    property Height;
    property Hint;
    property Left;
    property Margins;
    property MaxLength;
    property Name;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ReadOnly;
    property ShowHint;
    property StyleElements;
    property TabOrder;
    property TabStop;
    property Text;
    property Top;
    property Visible;
    property Width;
    property OnChange;
    { publish events }
    property OnClick;
    property OnDblClick;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('DataCAD', [TDcadNumericEdit]);
end;

constructor TDcadNumericEdit.Create(AOwner: TComponent);
begin
  inherited;
  validClr := Color;
  errorClr := clRed;
  initialTxt := '';
end;


procedure TDcadNumericEdit.AngToStr (ang : double;
                                     var str : OpenString;
                                     rel, roundit : boolean);
var
  i : integer;
begin
  i := pgSaveVar^.AngStyl;
  if datatype = DegMinSec then
    pgSaveVar^.AngStyl := 0
  else if datatype = DecDegrees then
    pgSaveVar^.AngStyl := 3;

  Ang2Str (ang, str, rel, roundit);

  pgSaveVar^.AngStyl := i;
end;


procedure TDcadNumericEdit.SetClr (isValid : boolean);
var
  brightness : double;
begin
  if isValid then
    Color := validClr
  else
    Color := ColorError;

  brightness := sqrt(0.241*sqr(GetRValue(ColorToRGB(Color))) +
                     0.691*sqr(GetGValue(ColorToRGB(Color))) +
                     0.068*sqr(GetBValue(ColorToRGB(Color))));
  if brightness > 127 then
    font.Color := clBlack
  else
    font.Color := clWhite;
end;

function TDcadNumericEdit.isValid : boolean;
var
  i : integer;
  ss : shortstring;
  initialNum : double;
begin
  initialNum := number;
  try
    result := true;
    case datatype of
      Distance : result := toDis (shortstring(text), number);
      DegMinSec : result := Str2Ang (shortstring(text), true, number);
      DecDegrees : number := StrToFloat(text)*Pi/180;
      IntegerNum : begin
          i := StrToInt (text);
          number := i;
        end
      else number := StrToFloat(text);
    end;
  except
    result := false;
  end;
  if result and (initialTxt = Text) then
    number := initialNum; // don't compound errors by converting original num to a string and then back to a number
if result and not Focused then begin
    // redisplay correctly formatted text
    // (some unusual strings actually return a true result with ToDis and
    //  Str2Ang, so redisplay with correct format)
    case datatype of
      Distance : begin
          tostr (number, ss, false);
          text := string(ss);
        end;
      DegMinSec : begin
          AngToStr (number, ss, true, false);
          text := string(ss);
        end;
      DecDegrees : text := FloatToStr (number*180/Pi);
      IntegerNum : text := IntToStr (round(number));
      else  text := FloatToStr (number);
    end;
    if True then
    

  end;
end;

procedure TDcadNumericEdit.setNumber(num: Double);
var
  ss : shortstring;
begin
  number := num;
  if not allowNeg then number := abs(number);

  case datatype of
    Distance : begin
        tostr (number, ss, false);
        text := string (ss);
      end;
    DegMinSec : begin
        AngToStr (number, ss, true, false);
        text := string (ss);
      end;
    DecDegrees : text := FloatToStr (number*180/Pi);
    IntegerNum : text := IntToStr (round(num));
    else  text := FloatToStr (number);
  end;

  initialtxt := text;

  SetClr (true);
end;

procedure TDcadNumericEdit.setNumber(num: integer);
var
  ss : shortstring;
begin
  number := num;
  if not allowNeg then number := abs(number);

  case datatype of
    Distance : begin
        tostr (number, ss, false);
        text := string (ss);
      end;
    DegMinSec : begin
        AngToStr (number, ss, true, false);
        text := string (ss);
    end;
    DecDegrees : text := FloatToStr (number*180/Pi);
    else text := IntToStr (round(number));
  end;

  SetClr (true);
end;


function TDcadNumericEdit.getNumber : Double;
begin
  if isValid then begin
    if datatype = IntegerNum then
      result := double(round(number))
    else
      result := number;

    if not allowNeg then
      result := abs(result);
  end
  else
    result := 0.0;
end;


function TDcadNumericEdit.getInt : integer;
begin
  if isValid then begin
    result := round(number);
    if not allowNeg then
      result := abs(result);
  end
  else
    result := 0;
end;


procedure TDcadNumericEdit.KeyPress(var Key: Char);
begin
  if not CharInSet(key, ['0'..'9', '-', FormatSettings.DecimalSeparator , '/', #8, #13, #127, #3, #$16]) then
    key := #0;

  if not allowNeg then
    if key = '-' then key := #0;

  if ((PGSaveVar^.scaletype <> 0) and (PGSaveVar^.scaletype <> 4))
  or (datatype <> Distance) then begin
    if key = '/' then key := #0;
  end;

  if (datatype = IntegerNum) and (key = FormatSettings.DecimalSeparator) then
    key := #0;

  if key = #0 then System.SysUtils.beep;

  inherited KeyPress(Key)
end;

procedure TDcadNumericEdit.DoExit;
begin
  SetClr (valid);

  inherited DoExit;
end;

procedure TDcadNumericEdit.DoEnter;
begin
  initialTxt := Text;
  inherited DoEnter;
end;

end.
